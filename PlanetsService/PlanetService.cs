﻿using PlanetsData.Models;
using System.Collections.Generic;

namespace PlanetsService
{
    public class PlanetService
    {
        public List<Planet> GetDefaultPlanets()
        {
            List<Planet> planetList = new List<Planet>();

            planetList.Add(new Planet
            {
                PlanetId = 1,
                Name = "Mercury",
                Diameter = 3031,
                Mass = 0.33M,
                DistanceFromSun = 36000,
                Gravity = 3.7M,
                LengthOfDay = 4222.2M,
                NumberOfMoons = 0
            });

            planetList.Add(new Planet
            {
                PlanetId = 2,
                Name = "Venus",
                Diameter = 7521,
                Mass = 4.87M,
                DistanceFromSun = 67000,
                Gravity = 8.9M,
                LengthOfDay = 2802,
                NumberOfMoons = 0
            });

            planetList.Add(new Planet
            {
                PlanetId = 3,
                Name = "Earth",
                Diameter = 7926,
                Mass = 5.97M,
                DistanceFromSun = 92960,
                Gravity = 9.8M,
                LengthOfDay = 24,
                NumberOfMoons = 1
            });

            planetList.Add(new Planet
            {
                PlanetId = 4,
                Name = "Mars",
                Diameter = 4222M,
                Mass = 0.642M,
                DistanceFromSun = 141700,
                Gravity = 3.7M,
                LengthOfDay = 24.7M,
                NumberOfMoons = 2
            });

            planetList.Add(new Planet
            {
                PlanetId = 5,
                Name = "Jupiter",
                Diameter = 88846,
                Mass = 1898,
                DistanceFromSun = 483500,
                Gravity = 23.1M,
                LengthOfDay = 9.9M,
                NumberOfMoons = 79
            });

            planetList.Add(new Planet
            {
                PlanetId = 6,
                Name = "Saturn",
                Diameter = 74900,
                Mass = 568,
                DistanceFromSun = 888750,
                Gravity = 9.0M,
                LengthOfDay = 9,
                NumberOfMoons = 62
            });

            planetList.Add(new Planet
            {
                PlanetId = 7,
                Name = "Uranus",
                Diameter = 31763,
                Mass = 86.8M,
                DistanceFromSun = 1783744,
                Gravity = 8.7M,
                LengthOfDay = 17.2M,
                NumberOfMoons = 27
            });

            planetList.Add(new Planet
            {
                PlanetId = 8,
                Name = "Neptune",
                Diameter = 30779,
                Mass = 102,
                DistanceFromSun = 2797770,
                Gravity = 11,
                LengthOfDay = 16.1M,
                NumberOfMoons = 14
            });

            planetList.Add(new Planet
            {
                PlanetId = 9,
                Name = "Pluto",
                Diameter = 1473,
                Mass = 0.0146M,
                DistanceFromSun = 3670046,
                Gravity = 0.7M,
                LengthOfDay = 153.3M,
                NumberOfMoons = 5
            });

            return planetList;
        }
    }
}
