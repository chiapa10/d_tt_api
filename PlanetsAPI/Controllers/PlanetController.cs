﻿using Microsoft.AspNetCore.Mvc;
using PlanetsData.Models;
using PlanetsService;
using System.Collections.Generic;
using System.Linq;

namespace PlanetsAPI.Controllers
{
    [Route("api/[controller]")]
    public class PlanetController : Controller
    {
        private readonly PlanetService _planetService = new PlanetService();

        // static list to hold data: this would normally be stored in a database
        // but for the purpose of this test, it is stored in this static variable
        private static List<Planet> _planetList = new List<Planet>();
        
        // GET api/planet
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(true);
        }

        // GET api/planet/get
        [HttpGet("get")]
        public IActionResult GetPlanets()
        {
            return Ok(_planetList);
        }

        // GET api/planet/reset
        [HttpGet("reset")]
        public IActionResult GetDefaultPlanets()
        {
            _planetList = _planetService.GetDefaultPlanets();

            return Ok(_planetList);
        }

        // GET api/planet/detail
        [HttpGet("detail")]
        public IActionResult GetPlanetDetail(int id)
        {

            Planet planet = _planetList.FirstOrDefault(p => p.PlanetId == id);

            return Ok(planet);
        }

        // POST api/planet/save
        [HttpPost("save")]
        public IActionResult SavePlanetDetail([FromBody] Planet planet)
        {
            Planet oldPlanet = _planetList.Where(p => p.PlanetId == planet.PlanetId).FirstOrDefault();
            var index = _planetList.IndexOf(oldPlanet);

            if (index != -1)
            {
                _planetList[index] = planet;
            }

            return Ok(planet);
        }
    }
}
