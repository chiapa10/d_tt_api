using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlanetsData.Models;
using PlanetsService;
using System.Collections.Generic;

namespace PlanetsTests
{
    [TestClass]
    public class PlanetServiceTests
    {
        private PlanetService _planetService;

        [TestInitialize]
        public void TestInitialize()
        {
            _planetService = new PlanetService();
        }

        private PlanetService CreateService()
        {
            return new PlanetService();
        }

        [TestMethod]
        public void GetPlanets_IsOfTypeListOfPlanet_True()
        {
            // Arrange
            var unitUnderTest = CreateService();

            // Act
            var result = unitUnderTest.GetDefaultPlanets();

            // Assert
            Assert.IsInstanceOfType(result, typeof(List<Planet>));
        }
    }
}
